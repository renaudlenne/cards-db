defmodule CardsDb.Main.Card do
  use Ecto.Schema
  import Ecto.Changeset

  schema "cards" do
    field :content, :string
    field :image, CardsDbWeb.CustomImageField
    field :lore, :string
    field :name, :string

    belongs_to :release, CardsDb.Main.Release
    belongs_to :card_type, CardsDb.Main.CardType
    belongs_to :phase, CardsDb.Main.Phase

    timestamps()
  end

  @doc false
  def changeset(card, attrs) do
    full_attrs = if Map.has_key?(attrs, "image") do
      {:ok, upload} = Cloudex.upload(attrs["image"].path)
      %{attrs | "image"=> upload.public_id}
    else
      attrs
    end
    card
    |> cast(full_attrs, [:name, :image, :release_id, :card_type_id, :phase_id, :content, :lore])
    |> validate_required([:name, :image, :release_id, :card_type_id, :content])
  end
end
