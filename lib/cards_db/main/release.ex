defmodule CardsDb.Main.Release do
  use Ecto.Schema
  import Ecto.Changeset

  schema "releases" do
    field :date, :naive_datetime
    field :name, :string

    has_many :cards, CardsDb.Main.Card

    timestamps()
  end

  @doc false
  def changeset(release, attrs) do
    release
    |> cast(attrs, [:name, :date])
    |> validate_required([:name, :date])
  end
end
