defmodule CardsDb.Main.CardType do
  use Ecto.Schema
  import Ecto.Changeset

  schema "card_types" do
    field :name, :string

    has_many :cards, CardsDb.Main.Card

    timestamps()
  end

  @doc false
  def changeset(card_type, attrs) do
    card_type
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
