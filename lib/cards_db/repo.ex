defmodule CardsDb.Repo do
  use Ecto.Repo,
    otp_app: :cards_db,
    adapter: Ecto.Adapters.Postgres
end
